#!/bin/env python3
import requests
import os
import sqlite3
from sys import stderr
from lxml import etree
from argparse import ArgumentParser

SITE_ROOT       = 'https://tvtropes.org'
CONTENT_ROOT    = SITE_ROOT    + '/pmwiki'
NAMESPACE_INDEX = CONTENT_ROOT + '/namespace_index.php'
ARTICLE_COUNT   = CONTENT_ROOT + '/articlecount.php'
WIKI_ROOT       = CONTENT_ROOT + '/pmwiki.php'
NS_PARAM        = 'ns='
PAGE_PARAM      = 'page='
QUERY_START     = '?'
QUERY_ADD       = '&'
DIR_SEP         = '/'
URL_SEP         = '/'
DIR_SEP_ESC     = '%'
CURRENT_DIR     = '.'
CACHE_DIR_NAME  = 'tropesbase'

html_parser = etree.HTMLParser()


def main():
    parser = ArgumentParser(
        prog='tropesbase',
        description='A command line query tool for TVTropes.org'
    )
    parser.add_argument('-n', '--namespace',
        help='By itself, query the articles in the namespace. Used with --article, to get a namespace other than Main'
    )
    parser.add_argument('-l', '--limit',
        help='Limit the result set to the provided namespace'
    )
    parser.add_argument('-a', '--article',
        help='The id of the article to query for links'
    )
    parser.add_argument('-t', '--titles',
        action='store_true',
        help='Used with --namespace prints titles instead of ids'
    )
    parser.add_argument('--generate',
        action='store_true',
        help='Generate the tropes.db sqlite database. WARNING: takes awhile'
    )
    parser.add_argument('-s', '--similar',
        action='store_true',
        help='Get articles similar (by linked tropes) to the given article'
    )
    args = parser.parse_args()

    if args.generate:
        generate_db()
        exit(0)

    if args.similar:
        get_similar(args.namespace, args.article)
        exit(0)

    if args.article is not None:
        namespace = args.namespace if args.namespace is not None else 'Main'
        url = WIKI_ROOT + f'/{namespace}/{args.article}'
        for link in get_wiki_links(url, limit_ns=args.limit):
            print(f'{link["link_ns"]}/{link["link_id"]}')
    elif args.namespace is not None:
        for article in get_articles(args.namespace):
            if article is None:
                continue
            print(article['title' if args.titles else 'id'])
    else:
        for ns in get_namespaces():
            print(ns)
    exit(0)
# end main


def generate_db():
    conn = sqlite3.connect('tropes.db')
    cur  = conn.cursor()
    cur.executescript('''
        create table if not exists articles(
            ns varchar collate nocase,
            id varchar collate nocase,
            title varchar,
            primary key(ns, id) on conflict ignore
        );
        create table if not exists links(
            ns varchar collate nocase,
            id varchar collate nocase,
            lns varchar collate nocase,
            lid varchar collate nocase,
            primary key(ns, id, lns, lid) on conflict ignore,
            foreign key(ns, id) references articles on delete cascade,
            foreign key(lns, lid) references articles(ns, id) on delete cascade
        );
    ''')

    for ns in get_namespaces():
        for article in get_articles(ns):
            if not cur.execute('select * from articles where ns = :namespace and id = :id', article).fetchone() is None:
                continue  # skip if we've already done this article

            cur.execute('insert into articles values(:namespace, :id, :title)', article)
            for link in get_wiki_links(article['url']):
                article['link_ns'] = link['link_ns']
                article['link_id'] = link['link_id']
                cur.execute('insert into links values(:namespace, :id, :link_ns, :link_id)', article)
            conn.commit()
# end generate_db


def patch_missing():
    conn = sqlite3.connect('tropes.db')
    cur  = conn.cursor()
    missing = cur.execute('''
                select distinct lns, lid
                from links
                where not exists (
                    select *
                    from articles
                    where articles.ns = links.lns
                    and articles.id   = links.lid
                )
                ''').fetchall()
    if len(missing) == 0:
        return False
    for m in missing:
        article = get_article(m[0], m[1])
        cur.execute('insert into articles values(:namespace, :id, :title)', article)
        for link in get_wiki_links(article['url']):
            article['link_ns'] = link['link_ns']
            article['link_id'] = link['link_id']
            cur.execute('insert into links values(:namespace, :id, :link_ns, :link_id)', article)
        conn.commit()
    return True
# end patch_missing


def get_similar(namespace: str, article: str) -> list[str]:
    url = get_url(namespace, article)
    similars = {}
    for link in get_wiki_links(url, limit_ns='Main'):
        link_url = get_url(link['link_ns'], link['link_id'])
        for link_link in get_wiki_links(link_url):
            key = f'{link_link["link_ns"]}/{link_link["link_id"]}'
            if key in similars:
                similars[key] += 1
            else:
                similars[key]  = 1
    print(similars)
# end get_similar


def get_url(namespace: str, article: str) -> str:
    return WIKI_ROOT + namespace + URL_SEP + article
# end get_url


def first_elem_with_attr_has(html: etree.ElementTree, elem: str, attr: str, has: str):
    for e in html.iter(elem):
        this_attr = e.get(attr)
        if this_attr and has in this_attr.split():
            return e
    return None
# end first_elem_with_attr_has


def get_page_count(url: str) -> int:
    html = etree_from_cache(url)
    if html is None:
        return None
    if (nav := first_elem_with_attr_has(html, 'nav', 'class', 'pagination-box')) is None:
        return 1
    if not (pages_attr := nav.get('data-total-pages')):
        return 1
    if not pages_attr.isdigit():
        return 1
    return int(pages_attr)
# end get_page_count


def get_namespaces() -> list[str]:
    page_count = get_page_count(ARTICLE_COUNT)
    namespaces = []
    for i in range(1, page_count + 1):
        ns_list_url  = ARTICLE_COUNT + QUERY_START + PAGE_PARAM + str(i)
        ns_list_doc  = etree_from_cache(ns_list_url)
        if ns_list_doc is None:
            continue
        main_content = first_elem_with_attr_has(ns_list_doc, 'div', 'id', 'wikimiddle')
        for text in main_content[-1].itertext():
            li = text.split(':')
            if li[0].isdigit() and len(li) == 2:
                namespaces.append(li[1].strip())
    return namespaces
# end get_namespaces


def get_articles(namespace: str):
    namespace_index = NAMESPACE_INDEX + QUERY_START + NS_PARAM + namespace
    page_count = get_page_count(namespace_index)
    articles   = []
    for i in range(1, page_count + 1):
        article_list_url = namespace_index + QUERY_ADD + PAGE_PARAM + str(i)
        article_list_doc = etree_from_cache(article_list_url)
        if article_list_doc is None:
            break
        main_content     = first_elem_with_attr_has(article_list_doc, 'div', 'class', 'column')
        if main_content is None:
            continue
        for a in twikilinks(main_content):
            articles.append({
                "namespace": namespace,
                "id":        a.get('title').split('/')[-1],
                "title":     a.text,
                "url":       a.get('title').replace('http', 'https')
            })
    return articles
# end get_articles


def get_article(ns, name):
    url   = WIKI_ROOT + f'/{ns}/{name}'
    html  = etree_from_cache(url)
    if html is not None:
        title_elem = first_elem_with_attr_has(html, 'h1', 'class', 'entry-title')
        title = "".join(
                    title_elem.itertext()
                ).split('/')[-1].strip() if title_elem is not None else None
    else:
        title = None
    return {
        "namespace": ns,
        "id": name,
        "url": url,
        "title": title
    }
# end get_article


def get_wiki_links(url, limit_ns=None):
    html = etree_from_cache(url)
    if html is None:
        return []
    wiki_links = []
    for link in twikilinks(html):
        link_parts = link.get('href').split('/')
        if len(link_parts) < 2:
            continue
        link_ns = link_parts[-2]
        if limit_ns is not None and link_ns != limit_ns:
            continue
        article = link_parts[-1]
        wiki_links.append({
            "link_ns": link_ns,
            "link_id": article
        })
    return wiki_links
# end get_wiki_links


def twikilinks(html: etree.ElementTree):
    return (elem for elem in html.iter('a') if elem.get('class') and 'twikilink' in elem.get('class').split())
# end twikilinks


def get_cache_dir() -> str:
    return os.path.join(
        os.getenv(
            "XDG_CACHE_HOME",
            os.path.join(
                os.getenv("HOME", '/tmp'),
                '.cache'
            )
        ),
        CACHE_DIR_NAME
    )
# end get_cache_dir


def etree_from_cache(url: str) -> etree.ElementTree:
    cache_dir = get_cache_dir()
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)
    cache_file_name = url.replace(DIR_SEP, DIR_SEP_ESC)
    cache_file = os.path.join(cache_dir, cache_file_name).lower()
    if os.path.exists(cache_file):
        return etree.parse(cache_file, html_parser)
    while True:
        try:
            page = requests.get(url)
        except Exception as e:
            if type(e) is KeyboardInterrupt:
                raise
            print(f'exception {type(e)} "{e}": GET {url}', file=stderr)
            if type(e) is requests.exceptions.TooManyRedirects:
                return None
            continue
        if page.status_code != 200:
            print(f'failed with status code {page.status_code}: GET {url}', file=stderr)
            if page.status_code == 403 or page.status_code in range(500, 600):
                continue
        break
    with open(cache_file, 'x') as cache:
        cache.write(page.text)
    return etree.fromstring(page.text, html_parser)
# end etree_from_cache


if __name__ == "__main__":
    main()

